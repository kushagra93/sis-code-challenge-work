-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2019 at 12:06 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_clicks`
--

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `date_1` date NOT NULL,
  `category` varchar(300) NOT NULL,
  `employee_name` varchar(300) NOT NULL,
  `employee_address` varchar(300) NOT NULL,
  `expense_description` varchar(300) NOT NULL,
  `pre_tax_amount` int(25) NOT NULL,
  `tax_amount` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`date_1`, `category`, `employee_name`, `employee_address`, `expense_description`, `pre_tax_amount`, `tax_amount`) VALUES
('0000-00-00', 'category', 'employee_name', 'employee_address', 'expense_description', 0, 0),
('0000-00-00', 'Travel', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Taxi ride', 350, 31),
('0000-00-00', 'Meals and Entertainment', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Team lunch', 235, 18),
('0000-00-00', 'Computer - Hardware', 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', 'HP Laptop', 999, 75),
('0000-00-00', 'Computer - Software', 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', 'Microsoft Office', 899, 67),
('0000-00-00', 'Computer - Software', 'Natasha Romanoff', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iCloud Subscription', 50, 4),
('0000-00-00', 'Computer - Software', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iCloud Subscription', 50, 4),
('0000-00-00', 'Meals and Entertainment', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Coffee with Steve', 300, 23),
('0000-00-00', 'Travel', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Taxi ride', 230, 17),
('0000-00-00', 'Meals and Entertainment', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Client dinner', 200, 15),
('0000-00-00', 'Travel', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Flight to Miami', 200, 15),
('0000-00-00', 'Computer - Hardware', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Macbook Air', 1, 999),
('0000-00-00', 'Computer - Software', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Dropbox Subscription', 15, 1),
('0000-00-00', 'Travel', 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', 'Taxi ride', 200, 15),
('0000-00-00', 'Office Supplies', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Paper', 200, 15),
('0000-00-00', 'Meals and Entertainment', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Dinner with potential acquisition', 200, 15),
('0000-00-00', 'Computer - Hardware', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iPhone', 200, 15),
('0000-00-00', 'Travel', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Airplane ticket to NY', 200, 15),
('0000-00-00', 'Meals and Entertainment', 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', 'Starbucks coffee', 12, 1),
('0000-00-00', 'Travel', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Airplane ticket to NY', 1500, 113),
('0000-00-00', 'category', 'employee_name', 'employee_address', 'expense_description', 0, 0),
('0000-00-00', 'Travel', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Taxi ride', 350, 31),
('0000-00-00', 'Meals and Entertainment', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Team lunch', 235, 18),
('0000-00-00', 'Computer - Hardware', 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', 'HP Laptop', 999, 75),
('0000-00-00', 'Computer - Software', 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', 'Microsoft Office', 899, 67),
('0000-00-00', 'Computer - Software', 'Natasha Romanoff', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iCloud Subscription', 50, 4),
('0000-00-00', 'Computer - Software', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iCloud Subscription', 50, 4),
('0000-00-00', 'Meals and Entertainment', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Coffee with Steve', 300, 23),
('0000-00-00', 'Travel', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Taxi ride', 230, 17),
('0000-00-00', 'Meals and Entertainment', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Client dinner', 200, 15),
('0000-00-00', 'Travel', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Flight to Miami', 200, 15),
('0000-00-00', 'Computer - Hardware', 'Steve Rogers', '783 Park Ave New York NY 10021', 'Macbook Air', 1, 999),
('0000-00-00', 'Computer - Software', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Dropbox Subscription', 15, 1),
('0000-00-00', 'Travel', 'Nick Fury', '1 Infinite Loop Cupertino CA 95014', 'Taxi ride', 200, 15),
('0000-00-00', 'Office Supplies', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Paper', 200, 15),
('0000-00-00', 'Meals and Entertainment', 'Stephen Strange', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Dinner with potential acquisition', 200, 15),
('0000-00-00', 'Computer - Hardware', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'iPhone', 200, 15),
('0000-00-00', 'Travel', 'Tony Stark', '1 Infinite Loop Cupertino CA 95014', 'Airplane ticket to NY', 200, 15),
('0000-00-00', 'Meals and Entertainment', 'Bruce Banner', '1 Infinite Loop Cupertino CA 95014', 'Starbucks coffee', 12, 1),
('0000-00-00', 'Travel', 'Carol Danvers', '1600 Amphitheatre Parkway Mountain View CA 94043', 'Airplane ticket to NY', 1500, 113);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `password`, `date_added`) VALUES
(8, 'Kushagra', 'Sahni', 'kushagra.sahni93@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '2019-05-05 11:05:53'),
(9, '', '', 'ved@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:08:02'),
(10, 'ved', 'dwivedi', 'ved@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:19:51'),
(11, 'ved', 'dwivedi', 'v@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:25:26'),
(12, 'as', 'as', 'ved@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:25:44'),
(13, 'Kushagra', 'Sahni', 'kushagra.sahni@progressive.in', 'f970e2767d0cfe75876ea857f92e319b', '2019-05-06 05:30:14'),
(14, 'Kushagra', 'Sahni', 'kushagra.sahni@progressive.in', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:32:08'),
(15, 'ved', 'd', 'ved@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 05:32:18'),
(16, 'ved', 'dwivedi', 'ved2@gmail.com', '202cb962ac59075b964b07152d234b70', '2019-05-06 08:42:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
