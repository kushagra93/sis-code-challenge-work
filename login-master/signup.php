<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Login - Bootstrap Admin Template</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />

    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/pages/signin.css" rel="stylesheet" type="text/css">
  <style>
#error-msg{ display:none }
#success-msg{ display:none }
</style>
</head>

<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="index.html">
				Bootstrap Admin Template				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="signup.php" class="">
							Don't have an account?
						</a>
						
					</li>
					
				
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container">
	
	<div class="content clearfix">
		
		<form name="sign-up-form"  id="sign-up-form" method="post" action="register.php">
		
			<h1>Sign Up</h1>	
            <div class="alert" id="error-msg">
            
           </div>
           
           <div class="alert alert-success" id="success-msg">
            
          </div>	
			
			<div class="login-fields">
				
				<p>Please provide your details</p>

                <div class="field">
                    <label for="username">First Name</label>
                    <input type="text" id="first" name="first"  placeholder="First Name" class="login username-field" />
                </div>
                <div class="field">
                    <label for="username">Last Name</label>
                    <input type="text" id="last" name="last"  placeholder="Last Name" class="login username-field" />
                </div>
				<div class="field">
					<label for="username">Email</label>
					<input type="text" id="email" name="email"  placeholder="Email" class="login username-field" />
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Password:</label>
					<input type="password" id="password_1" name="password_1"  placeholder="Password" class="login password-field"/>
				</div> 
				<div class="field">
					<label for="password">Confirm Password:</label>
					<input type="password" id="password_2" name="password_2"  placeholder="Confirm Password" class="login password-field"/>
				</div><!-- /password -->
				
			</div> <!-- /login-fields -->
			<div class="input-group">
		  	  <button type="submit" class="button btn btn-success btn-large" name="reg_user">Register</button>
		  	</div>
		  	<p>
  			Already a member? <a href="login.php">Sign in</a>
  			</p>
			
			
			
		</form>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->





<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/bootstrap.js"></script>

<script src="js/login.js"></script>

</body>

</html>
