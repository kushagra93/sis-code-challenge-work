<?php
session_start();

$db= mysqli_connect('localhost','root','','php_clicks');

if (isset($_POST['reg_user'])) {
    $first = mysqli_real_escape_string($db, $_POST['first']);
    $last = mysqli_real_escape_string($db, $_POST['last']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

    if (empty($first)) { array_push($errors, "First Name is required"); }
    if (empty($last)) { array_push($errors, "Last Name is required"); }
    if (empty($email)) { array_push($errors, "Email is required"); }
    if (empty($password_1)) { array_push($errors, "Password is required"); }
    if ($password_1 != $password_2) {
        array_push($errors, "The two passwords do not match");
    }


    // first check the database to make sure
    // a user does not already exist with the same username and/or email
    $user_check_query = "SELECT * FROM users WHERE email='$email' LIMIT 1";
    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);

    if ($user['email'] === $email) {
        array_push($errors, "email already exists");
    }

    if (count($errors) == 0) {
        $password = md5($password_1);//encrypt the password before saving in the database


        $query = "INSERT INTO users (first_name, last_name, email, password)  VALUES('$first','$last','$email', '$password')";
        mysqli_query($db, $query);
        $_SESSION['email'] = $email;
        $_SESSION['success'] = "You are now logged in";
        header('location: dashboard.php');
    }
}

?>